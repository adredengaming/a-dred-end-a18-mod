Key,UsedInMainMenu,Source,Context,Changes,English,French,German,Klingon,Spanish,Polish

BicycleStand,,blocks,Block,New,Bicycle Stand,Support à bicyclette,Fahrradständer,,Puesto de bicicletas,Stojak na rowery
BicycleStandDesc,,blocks,Block,New,A stand where a bicycle would go,Un stand où un vélo irait,"Ein Stand, wo ein Fahrrad würde",,Un stand donde iría una bicicleta,Podstawka gdzie rower pójdzie
BicycleOnStand,,blocks,Block,New,Bicycle on stand,Vélo sur le stand,Fahrrad auf dem Stand,,Bicicletas en el stand,Rower na stojaku
BicycleOnStandDesc,,blocks,Block,New,A bicycle in a stand,Une bicyclette dans une position,Ein Fahrrad in einem Ständer,,Una bicicleta en un banco,Rower w stojaku