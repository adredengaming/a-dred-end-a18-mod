Key,UsedInMainMenu,Source,Context,Changes,English,French,German,Klingon,Spanish,Polish

Idol,,,,,A small stone idol,,,,,
IdolDesc,,,,,/"A monster of vaguely anthropoid outline, but with an octopus-like head whose face was a mass of feelers, a scaly, rubbery-looking body, prodigious claws on hind and fore feet, and long, narrow wings behind/",,,,,

IdolHuge,,,,,A small stone idol With HUGE CTHULHU,,,,,
IdolHugeDesc,,,,,/"A monster of vaguely anthropoid outline, but with an octopus-like head whose face was a mass of feelers, a scaly, rubbery-looking body, prodigious claws on hind and fore feet, and long, narrow wings behind/"/, But HUGE in the distance,,,,,